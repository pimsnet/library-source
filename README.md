library.app Symfony Client App
=======

A Symfony project created on 16-10-2017

To run:

```
composer install
php bin/console doctrine:database:create
php bin/console doctrine:schema:create
php bin/console doctrine:load
php bin/console cache:clear --env=prod --no-debug
```