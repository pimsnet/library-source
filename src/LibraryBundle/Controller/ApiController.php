<?php

namespace LibraryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// Get Route Definition
use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiController extends Controller
{
    /**
     * GET Route annotation.
     * @Get("api/books/available")
     */
    public function getBooksAvailableAction()
    {

        $em = $this->getDoctrine()->getManager();
        $books = $em->createQuery(
            'SELECT b
            FROM LibraryBundle:Book b
            WHERE b.amount > :amount'
        )->setParameter('amount', 0);

        $serializer = $this->get('jms_serializer');
        $json = $serializer->serialize(['books'=>$books->getResult()], 'json');

        return new JsonResponse(json_decode($json));
    }

    /**
     * GET Route annotation.
     * @Get("api/books/notavailable")
     */
    public function getBooksNotAvailableAction()
    {

        $em = $this->getDoctrine()->getManager();
        $books = $em->createQuery(
            'SELECT b
            FROM LibraryBundle:Book b
            WHERE b.amount = :amount'
        )->setParameter('amount', 0);

        $serializer = $this->get('jms_serializer');
        $json = $serializer->serialize(['books'=>$books->getResult()], 'json');

        return new JsonResponse(json_decode($json));
    }

    /**
     * GET Route annotation.
     * @Get("api/books/amount/{amount}")
     */
    public function getBooksAmount($amount)
    {
        $em = $this->getDoctrine()->getManager();
        $books = $em->createQuery(
            'SELECT b
            FROM LibraryBundle:Book b
            WHERE b.amount >= :amount'
        )->setParameter('amount', $amount);

        $serializer = $this->get('jms_serializer');
        $json = $serializer->serialize(['books'=>$books->getResult()], 'json');

        return new JsonResponse(json_decode($json));
    }
}
