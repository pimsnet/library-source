<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use LibraryBundle\Entity\Book;

class Fixtures extends Fixture
{

    public $books = [
      ['title'=>'Book 1','amount'=>0],
      ['title'=>'Book 2','amount'=>5],
      ['title'=>'Book 3','amount'=>12],
      ['title'=>'Book 4','amount'=>2],
    ];
    public function load(ObjectManager $manager)
    {

        foreach ($this->books as $book)
        {
            $bookEntity = new Book();
            $bookEntity->setTitle($book['title']);
            $bookEntity->setAmount($book['amount']);
            $manager->persist($bookEntity);
        }

        $manager->flush();
    }
}